def print_header(string):
    """
    Prints header
    :param string:
    :return:
    """
    print("-" * (len(string) + 4))
    print("  %s  " % string)
    print("-" * (len(string) + 4))

def print_banner(string):
    """
    :param string:
    :return: A header to print
    """

    lines = string.splitlines()
    max_ln = max([len(s) for s in lines])
    print("*" * (max_ln + 6))
    for ln in lines:
        print("*  %s  *" % ln.center(max_ln))
    print("*" * (max_ln + 6))

def print_msg(string):
    """
    :param string:
    """
    print("*)  %s  " % string)

def print_num_msg(idx, string):
    """
    Prints a message with a number
    :param idx
    :param string:
    """
    print("%s)  %s  " % (str(idx).rjust(3),string))

def print_key_value_msg(idx, string, size=30):
    """
    Prints a message with a key padded with size spaces
    :param idx
    :param string:
    :param size
    """
    print("%s:  %s  " % (str(idx).ljust(size),string))