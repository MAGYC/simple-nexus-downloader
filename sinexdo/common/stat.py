import os

def make_executable(path):
    """Adds executable flag to entities with read access"""
    mode = os.stat(path).st_mode
    read_perms_to_exec = (mode & 0o444) >> 2
    os.chmod(path, mode | read_perms_to_exec)
