import pytest


from cmd import Cmd

def test_parser_minimal_args():
    # --Given--
    cmd = Cmd()

    # --When--
    arguments = cmd._parse("info --nexus_url http://somewhere.org --repository maven-releases --group fr.magyc --artifact foo.bar".split())

    # --Then--
    assert arguments.action == 'info'
    assert arguments.nexus_url == "http://somewhere.org"
    assert arguments.artifact == 'foo.bar'
    assert arguments.group == 'fr.magyc'

def test_parser_wo_url():
    # --Given--
    cmd = Cmd()

    # --When--
    with pytest.raises(SystemExit) as system_exit_exception:
        cmd._parse("--nexus_url".split())

    # --Then--
    assert system_exit_exception.type == SystemExit
    assert system_exit_exception.value.code == 2


def test_action(monkeypatch):
    # --Given--
    cmd = Cmd()
    args = "info --nexus_url http://somewhere.org --repository maven-releases --group fr.magyc --artifact foo.bar".split()

    # --When--
    witness=[]
    def info():
        witness.append(True)

    monkeypatch.setattr(cmd,'info',info)

    cmd.run(args)

    # --Then--
    assert witness[0]


def test_snapshot_opt_off(monkeypatch):
    # --Given--
    cmd = Cmd()
    args = "info --nexus_url http://somewhere.org --repository maven-releases --group fr.magyc --artifact foo.bar --version 0.0.1".split()

    def info():
        pass
    monkeypatch.setattr(cmd,'info', info)

    # --When--
    cmd.run(args)

    # --Then--
    assert cmd.version == '0.0.1'

def test_snapshot_opt_on(monkeypatch):
    # --Given--
    cmd = Cmd()
    args = "info --nexus_url http://somewhere.org --repository maven-releases --group fr.magyc --artifact foo.bar --version 0.0.1 --snapshot".split()

    def info():
        pass
    monkeypatch.setattr(cmd,'info', info)

    # --When--
    cmd.run(args)

    # --Then--
    assert cmd.version == '0.0.1-SNAPSHOT'


def empty_result():
    """
    returns an empty requests result
    :return:
    """
    class Result:
        def __init__(self):
            self.url = 'foo'
            self.status_code = 404

        def json(self):
            return {'items': []}

    return Result()


def test_no_items(monkeypatch):

    # --Given--
    cmd = Cmd()
    args = "info --nexus_url http://somewhere.org --repository maven-releases --group fr.magyc --artifact foo.bar".split()

    def no_result(*args,**kwargs):
        return empty_result()

    import requests

    monkeypatch.setattr(requests,'get',no_result)

    # --When--
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        cmd.run(args)


    # --Then--