# Simple-nexus-downloader

Simple Nexus Downloader to get the last version of excutable jar

## Installation

Clone project from git

```bash
git clone https://git.ias.u-psud.fr/MAGYC/simple-nexus-downloader
cd simple-nexus-downloader
pip install -r requirement.txt
```

## Usage

Use `-h` option to get help

```
python cmd.py -h
usage: cmd.py [-h] --nexus_url NEXUS_URL --group GROUP --artifact ARTIFACT [--version VERSION] [--snapshot] --repository {maven-releases,maven-snapshots} [--extension EXTENSION] [--nature NATURE] [--filename FILENAME]
              [--service_name SERVICE_NAME] [--manage_service] [--log-file LOG_FILE] [--stored_version STORED_VERSION]
              {info,fetch,download}

Simple Nexus Downloader

positional arguments:
  {info,fetch,download}

optional arguments:
  -h, --help            show this help message and exit
  --nexus_url NEXUS_URL
                        Nexus server URL as 'https://repositories.forge.ias.u-psud.fr'
  --group GROUP         Maven group as 'fr.magyc'
  --artifact ARTIFACT   Artifact as 'uws-magyc-mongo'
  --version VERSION     Version
  --snapshot
  --repository {maven-releases,maven-snapshots}
                        Nexus repository
  --extension EXTENSION
  --nature NATURE
  --filename FILENAME
  --service_name SERVICE_NAME
                        The service name to manage (stop and start) service for downloading.
  --manage_service      Manage (stop and start) service for downloading.
  --log-file LOG_FILE   Print output to log file instead of stdout and stderr.
  --stored_version STORED_VERSION
```

To get an interactive dialog use `info` action.

 $  python3 sinexdo/cmd.py info --repository maven-snapshots  --nexus_url https://repositories.forge.ias.u-psud.fr --group fr.magyc --artifact uws-magyc-mongo  --version 0.0.1
 
 To download latest version use `download` action
 
 $  python3 sinexdo/cmd.py download --repository maven-snapshots --nexus_url https://repositories.forge.ias.u-psud.fr --group fr.magyc --artifact uws-magyc-mongo  --version 0.0.1
 



