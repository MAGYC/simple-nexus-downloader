from pathlib import Path
import argparse
import codecs
import contextlib
import os
import requests
import subprocess
import sys

from sinexdo.common.printools import print_banner, print_msg, print_header, print_num_msg, print_key_value_msg
from sinexdo.common.stat import make_executable

class Cmd:
    """
    Cmd is a line commnd to get last executable jar from repositories
    """
    def __init__(self):
        self._setup_parser()

    def _setup_parser(self):
        self.parser = argparse.ArgumentParser(description='Simple Nexus Downloader')
        self.parser.add_argument("--nexus_url",required=True,help="Nexus server URL as 'https://repositories.forge.ias.u-psud.fr'")
        self.parser.add_argument("--group", required=True, help="Maven group as 'fr.magyc'")
        self.parser.add_argument("--artifact", required=True, help="Artifact as 'uws-magyc-mongo'")
        self.parser.add_argument("--version", default=None, help="Version")
        self.parser.add_argument("--snapshot", action='store_true')
        self.parser.add_argument("--repository", required=True, choices=['maven-releases','maven-snapshots'], help="Nexus repository")
        self.parser.add_argument("--extension", default="jar")
        self.parser.add_argument("--nature", default="exec")
        self.parser.add_argument("action", choices=['info', 'fetch', 'download'])
        self.parser.add_argument("--filename", required=False, default="service.jar")
        self.parser.add_argument("--service_name", required=False,
                                 help="The service name to manage (stop and start) service for downloading.")
        self.parser.add_argument("--manage_service", required=False, action='store_true',
                                 help="Manage (stop and start) service for downloading.")
        self.parser.add_argument("--log-file", help="Print output to log file instead of stdout and stderr.")
        self.parser.add_argument("--stored_version", required=False, default="last-version.txt")

    def _run(self, args):
        print(args)

        action_method = getattr(self, self.action)
        action_method()

    def run(self, args):
        arguments = self._parse(args)

        self.group = arguments.group
        self.artifact = arguments.artifact
        self.version = arguments.version
        self.extension = arguments.extension
        self.api_url = arguments.nexus_url+"/service/rest/"
        self.action = arguments.action
        self.filename = arguments.filename
        self.stored_version = arguments.stored_version
        self.service_name = arguments.service_name
        self.manage_service = arguments.manage_service
        self.log_file = arguments.log_file
        self.repository = arguments.repository
        if arguments.snapshot and self.version and not self.version.endswith('SNAPSHOT') :
            self.version = self.version+'-SNAPSHOT'

        if self.log_file:
            with codecs.open(self.log_file, 'a', 'utf-8') as log:
                with contextlib.redirect_stdout(log), contextlib.redirect_stderr(log):
                    self._run(arguments)
        else:
            self._run(arguments)

    def _parse(self, args):
        return self.parser.parse_args(args)

    def fetch(self):
        return self._fetch_items()

    def _fetch_items(self):
        """
        Fetches artifacts from repository
        :return: a list of artifact
        """
        params = {'maven.groupId': self.group,
                  'maven.artifactId': self.artifact,
                  'maven.baseVersion': self.version,
                  'maven.extension': self.extension,
                  'repository': self.repository,
                  'sort': 'version'
                  }

        headers={"Accept": "application/json"}

        r = requests.get(self.api_url+'v1/search',params=params,headers=headers)
        self.called_url = r.url
        if r.status_code == 404 :
            print_banner('''/!\\\nNo returned result.\nCheck your parameters.''')
            print_header('Used parameters')
            for key, value in params.items():
                print_key_value_msg(key,value)
            sys.exit(-1)
        response = r.json()
        items =  response['items']
        items.sort(key=lambda i: i['version'], reverse=True)
        return items

    @staticmethod
    def repo_version(it, latest=False):
        version = it['version']
        s_rep = 20

        if latest:
            version = '> ' + version + ' < --(latest)'
            s_rep = s_rep - 2

        return it['repository'].ljust(s_rep) + version.ljust(30)

    def info(self):
        print_banner('Get informations about {}:{}:{}'.format(self.group,self.artifact,self.version,))

        items = self._fetch_items()

        selected_version_id = -1

        if items:
            print_header('Available versions')
            for idx, it in enumerate(items):
                to_print = self.repo_version(it, idx == 0)
                print_num_msg(idx + 1, to_print)

            selected_version_id = self.select_version_dialog(items)

        else:
            print_banner('''/!\\\nNo returned result.\nCheck your parameters.''')

        if selected_version_id >= 0:
            selected_items_assets = [ x['downloadUrl']
                               for x in items[selected_version_id]['assets']
                               if x['downloadUrl'].endswith(self.extension)]

            print_header('Assets URL')
            for idx, x in enumerate(selected_items_assets):
                print_num_msg(idx + 1, x)

            to_download_url = self.select_url_to_download(selected_items_assets)

            if to_download_url:
                self.do_download(to_download_url)



    @staticmethod
    def sizeof_fmt(num, suffix='o'):
        """
        To get a human readable fmt
        see (https://gist.github.com/cbwar/d2dfbc19b140bd599daccbe0fe925597 )
        :param num:
        :param suffix:
        :return:
        """
        for unit in ['', 'k', 'M', 'G', 'T', 'P', 'E', 'Z']:
            if abs(num) < 1024.0:
                return "%3.1f %s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Yi', suffix)

    def download(self):
        """
        Download last exec-jar
        :return:
        """
        items = self._fetch_items()

        if not items:
            print_banner('''/!\\\nNo returned result.\nCheck your parameters.''')
            sys.exit(-1)

        item = self.get_last_item(self._fetch_items())

        last_available_version = item['version']
        last_downloaded_version = self.get_last_downloaded_version()

        if last_available_version > last_downloaded_version:
            url = [ x['downloadUrl'] for x in item['assets'] if x['downloadUrl'].endswith('exec.jar')]

            if len(url) > 1:
                raise ValueError('Too many urls')
            asset_url = url[0]

            self.do_stop_service()
            self.do_download(asset_url)
            self.do_write_last_downloaded_version(last_available_version)
            self.do_start_service()
        else:
            print_banner("Last downloaded version %s is up to date " % last_downloaded_version)



    def get_last_item(self, items):
        """
        Get last item by date
        """
        return items[0] if items else None

    def do_download(self, url):
        file_path = self.resolve(self.filename)

        with file_path.open(mode='wb') as target:
            target.write(requests.get(url).content)
        make_executable(str(file_path))

        print_header('Download')
        print_msg('Downloaded file {} '.format(url))
        print_msg('Save to {} '.format(str(file_path)))
        print_msg("Size".ljust(10,'.')+Cmd.sizeof_fmt(os.path.getsize(str(file_path))))

    def do_write_last_downloaded_version(self,version):
        file_path = self.resolve(self.stored_version)
        with file_path.open(mode='wt') as target:
            target.write(version)
        print_msg("Last version %s saved to %s" % (version, str(file_path)))

    def get_last_downloaded_version(self):
        file_path = self.resolve(self.stored_version)
        if file_path.exists():
            with file_path.open(mode='r') as target:
                return target.read()
        return ""

    def select_version_dialog(self, items):
        while True:
            items_len = len(items)
            print('Choose a version from 1 to ' + str(items_len))
            answer = input('? (q to quit, m for latest version): ').strip()
            if answer == 'q':
                break
            if answer == 'm':
                selected_version_id = 0
                break
            elif answer.isdigit() and (int(answer) <= items_len):
                selected_version_id = int(answer) - 1
                break
        return selected_version_id

    def select_url_to_download(self, selected_items_assets):
        while True:
            print('Pick asset to download from 1 to ' + str(len(selected_items_assets)))
            answer = input('? (q to quit): ')
            if answer.strip() == 'q':
                break
            if answer.isdigit() and (int(answer) <= len(selected_items_assets)):
                return selected_items_assets[int(answer) - 1]
        return

    def resolve(self, filename):
        file_path = Path(filename)
        if not file_path.is_absolute():
            file_path = Path(os.getcwd(), filename)
        return file_path

    def do_stop_service(self):
        self.do_run_service_command('stop')

    def do_start_service(self):
        self.do_run_service_command('start')

    def do_run_service_command(self, command):
        if not self.manage_service :
            return
        if self.manage_service and not self.service_name :
            print_banner('''/!\\\nService name is missing .\nCheck service_name parameter.''')
            sys.exit(-1)
        with subprocess.Popen(['/usr/bin/sudo', '/bin/systemctl', command, self.service_name],stdout=subprocess.PIPE) as proc:
            print(proc.args, proc.returncode)


if __name__ == "__main__":
    cmd = Cmd()
    cmd.run(sys.argv[1:])
